FROM openjdk:11.0.7-jre-slim-buster
COPY build/libs/hello-world-0.1.0.jar app.jar
ENTRYPOINT ["java","-jar","./app.jar"]
